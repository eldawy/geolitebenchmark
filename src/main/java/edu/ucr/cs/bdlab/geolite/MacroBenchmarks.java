package edu.ucr.cs.bdlab.geolite;

import com.esri.core.geometry.ogc.OGCGeometry;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;
import scala.Tuple2;

import java.awt.geom.Rectangle2D;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MacroBenchmarks {

  enum Operation {OP_NONE, OP_RQ, OP_SJ}

  public static void main(String[] args) throws IOException {
    boolean discard = false;
    boolean reuse = false;
    int numGeometries = Integer.MAX_VALUE; // Virtually, no limit
    int numPointsPerGeometry = 50;
    MicroBenchmarks.CGLibrary library = MicroBenchmarks.CGLibrary.CG_GEOS;
    Operation operation = Operation.OP_NONE;
    JTSProcessor.CoordType coordType = JTSProcessor.CoordType.CT_ARRAY;
    String filename1 = null;
    String filename2 = null;

    if (args.length == 0) {
      System.err.println("Usage: main [inputfile] --operation=<op> [additional parameters]");
      System.err.println("operations {rq, sj}");
      System.err.println( "--reuse (--no-reuse) reuse objects only with geolite");
      System.err.println( "--coord=<ct> type of coordinate sequence {array, packeddouble, packedfloat}");
      System.err.println( "--library=<lib> the CG library to use {geolite, jts, esri}");
      System.exit(1);
    }

    for (String arg : args) {
      if (arg.startsWith("--")) {
        // Begins with two dashes. Identify a parameter
        int equalSign = arg.indexOf("=");
        String name, value;
        if (equalSign != -1) {
          name = arg.substring(2, equalSign);
          value = arg.substring(equalSign + 1);
        } else {
          name = arg.substring(2);
          value = "";
        }
        switch (name) {
          case "reuse": reuse = true; break;
          case "no-reuse": reuse = false; break;
          case "library":
            switch (value) {
              case "geolite": library = MicroBenchmarks.CGLibrary.CG_GEOLITE; break;
              case "esri": library = MicroBenchmarks.CGLibrary.CG_ESRI; break;
              case "jts": library = MicroBenchmarks.CGLibrary.CG_JTS; break;
              default: throw new RuntimeException("Unknown library "+value);
            }
            break;
          case "coord":
            switch (value) {
              case "array": coordType = JTSProcessor.CoordType.CT_ARRAY; break;
              case "packeddouble": coordType = JTSProcessor.CoordType.CT_PACKEDDOUBLE; break;
              case "packedfloat": coordType = JTSProcessor.CoordType.CT_PACKEDFLOAT; break;
              default: throw new RuntimeException("Unknown coordinate type "+value);
            }
            break;
          case "operation":
            switch (value) {
              case "rq": operation = Operation.OP_RQ; break;
              case "sj": operation = Operation.OP_SJ; break;
              default: throw new RuntimeException("Unknown operation "+value);
            }
        }
      } else {
        if (filename1 == null)
          filename1 = arg;
        else
          filename2 = arg;
      }
    }

    // Create the parser, filter, and wktizer according to the library type
    Function<String, Object> parser;
    Function<Object, Boolean> emptyFilter;
    Function<Object, Boolean> rangeFilter;
    Function<Object, String> wktizer;
    Function<Object, Tuple2<Object, Rectangle2D.Double>> mbrComputer;
    Function<Tuple2<Object, Object>, Boolean> refinement;
    Function<Tuple2<Object, Object>, Object> intersectionComputer;
    switch (library) {
      case CG_GEOLITE:
        boolean freuse = reuse;
        parser = new Function<String, Object>() {
          transient edu.ucr.cs.bdlab.wktparser.WKTParser parser = null;
          transient IGeometry geometry = null;

          @Override
          public Object call(String wkt) {
            if (parser == null)
              parser = new edu.ucr.cs.bdlab.wktparser.WKTParser();
            if (!freuse)
              geometry = null;
            try {
              return (geometry = parser.parse(wkt, geometry));
            } catch (edu.ucr.cs.bdlab.wktparser.ParseException e) {
              e.printStackTrace();
            } catch (IOException e) {
              e.printStackTrace();
            }
            return null;
          }
        };
        emptyFilter = geometry -> geometry != null && !((IGeometry) geometry).isEmpty();
        final IGeometry range1 = new edu.ucr.cs.bdlab.geolite.Envelope(2, -170, -30, -140, 30);
        rangeFilter = geometry -> ((IGeometry)geometry).intersects(range1);
        wktizer = geometry -> ((IGeometry)geometry).asText();
        mbrComputer = new Function<Object, Tuple2<Object, Rectangle2D.Double>>() {
          edu.ucr.cs.bdlab.geolite.Envelope e = new edu.ucr.cs.bdlab.geolite.Envelope(2);

          @Override
          public Tuple2<Object, Rectangle2D.Double> call(Object o) {
            ((IGeometry) o).envelope(e);
            return new Tuple2(o, new Rectangle2D.Double(e.minCoord[0], e.minCoord[1],
                e.maxCoord[0] - e.minCoord[0], e.maxCoord[1] - e.minCoord[1]));
          }

        };
        refinement = pair -> ((IGeometry)pair._1).intersects((IGeometry)pair._2);
        intersectionComputer = new Function<Tuple2<Object, Object>, Object>() {
          IGeometry geom = null;

          @Override
          public Object call(Tuple2<Object, Object> pair) {
            if (!freuse)
              geom = null;
            try {
              return geom = ((IGeometry) pair._1).intersection((IGeometry) pair._2, geom);
            } catch (Exception e) {
              return null;
            }
          }
        };
        break;
      case CG_ESRI:
        parser = wkt -> {
          try {
            return OGCGeometrySerializable.create(com.esri.core.geometry.ogc.OGCGeometry.fromText(wkt));
          } catch (Exception e) {
            return null;
          }
        };
        emptyFilter = geometry -> geometry != null && !((com.esri.core.geometry.ogc.OGCGeometry) geometry).isEmpty();
        rangeFilter = new Function<Object, Boolean>() {
          transient com.esri.core.geometry.ogc.OGCGeometry range;
          transient com.esri.core.geometry.SpatialReference ref;
          @Override
          public Boolean call(Object geometry) {
            if (ref == null)
              ref = com.esri.core.geometry.SpatialReference.create(4326);
            if (range == null) {

              range = com.esri.core.geometry.ogc.OGCGeometry.createFromEsriGeometry(
                  new com.esri.core.geometry.Envelope(-170, -30, -140, 30), ref);
            }
            try {
              return ((com.esri.core.geometry.ogc.OGCGeometry) geometry).intersects(range);
            } catch (Exception e) {
              return false;
            }

          }
        };
        wktizer = geometry -> ((com.esri.core.geometry.ogc.OGCGeometry)geometry).asText();
        mbrComputer = geometry -> {
          if (geometry == null)
            return new Tuple2(geometry, new double[] {Double.POSITIVE_INFINITY,
                Double.POSITIVE_INFINITY, Double.NEGATIVE_INFINITY, Double.NEGATIVE_INFINITY});
          com.esri.core.geometry.Envelope2D e = new com.esri.core.geometry.Envelope2D();
          ((OGCGeometry)geometry).envelope().getEsriGeometry().queryEnvelope2D(e);
          return new Tuple2(geometry, new Rectangle2D.Double(e.xmin, e.ymin, e.getWidth(), e.getHeight()));
        };
        refinement = pair -> {
          try {
            return ((com.esri.core.geometry.ogc.OGCGeometry)pair._1).intersects(
                ((com.esri.core.geometry.ogc.OGCGeometry)pair._2));
          } catch (Exception e) {
            return false;
          }
        };
        intersectionComputer = pair -> OGCGeometrySerializable.create(
            ((com.esri.core.geometry.ogc.OGCGeometry)pair._1).intersection(
            ((com.esri.core.geometry.ogc.OGCGeometry)pair._2)));
        break;
      case CG_JTS:
        JTSProcessor.CoordType fcoordtype = coordType;
        parser = new Function<String, Object>() {
          transient org.locationtech.jts.io.WKTReader reader;
          org.locationtech.jts.io.WKTReader getReader() {
            if (reader == null) {
              org.locationtech.jts.geom.GeometryFactory factory;
              switch (fcoordtype) {
                case CT_ARRAY:
                  factory = new org.locationtech.jts.geom.GeometryFactory(
                      new org.locationtech.jts.geom.PrecisionModel(org.locationtech.jts.geom.PrecisionModel.FLOATING), 4326,
                      org.locationtech.jts.geom.impl.CoordinateArraySequenceFactory.instance());
                  break;
                case CT_PACKEDDOUBLE:
                  factory = new org.locationtech.jts.geom.GeometryFactory(
                      new org.locationtech.jts.geom.PrecisionModel(org.locationtech.jts.geom.PrecisionModel.FLOATING), 4326,
                      org.locationtech.jts.geom.impl.PackedCoordinateSequenceFactory.DOUBLE_FACTORY);
                  break;
                case CT_PACKEDFLOAT:
                  factory = new org.locationtech.jts.geom.GeometryFactory(
                      new org.locationtech.jts.geom.PrecisionModel(org.locationtech.jts.geom.PrecisionModel.FLOATING_SINGLE), 4326,
                      org.locationtech.jts.geom.impl.PackedCoordinateSequenceFactory.FLOAT_FACTORY);
                  break;
                default:
                  throw new RuntimeException("Unknown coordinate type " + fcoordtype);
              }
              reader = new org.locationtech.jts.io.WKTReader(factory);
            }
            return reader;
          }

          @Override
          public Object call(String wkt) {
            try {
              return getReader().read(wkt);
            } catch (org.locationtech.jts.io.ParseException e) {
              System.err.println(String.format("Error parsing '%s'", wkt));
              e.printStackTrace();
            }
            return null;
          }
        };
        emptyFilter = geometry -> geometry != null && !((org.locationtech.jts.geom.Geometry)geometry).isEmpty();
        org.locationtech.jts.geom.Coordinate[] rangeCoordinates = {
            new org.locationtech.jts.geom.Coordinate(-170, -30),
            new org.locationtech.jts.geom.Coordinate(-140, -30),
            new org.locationtech.jts.geom.Coordinate(-140, 30),
            new org.locationtech.jts.geom.Coordinate(-170, 30),
            new org.locationtech.jts.geom.Coordinate(-170, -30),
        };
        org.locationtech.jts.geom.Geometry range3 = new org.locationtech.jts.geom.GeometryFactory().createPolygon(rangeCoordinates);
        rangeFilter = geom -> ((org.locationtech.jts.geom.Geometry) geom).intersects(range3);
        wktizer = geometry -> ((org.locationtech.jts.geom.Geometry) geometry).toText();
        mbrComputer = geometry -> {
          try {
            org.locationtech.jts.geom.Coordinate[] coords = ((org.locationtech.jts.geom.Geometry) geometry).getEnvelope().getCoordinates();
            double xmin, ymin, xmax, ymax;
            if (coords.length == 2) {
              xmin = Math.min(coords[0].x, coords[1].x);
              xmax = Math.max(coords[0].x, coords[1].x);
              ymin = Math.min(coords[0].y, coords[1].y);
              ymax = Math.max(coords[0].y, coords[1].y);
            } else {
              xmin = Math.min(coords[0].x, coords[2].x);
              xmax = Math.max(coords[0].x, coords[2].x);
              ymin = Math.min(coords[0].y, coords[2].y);
              ymax = Math.max(coords[0].y, coords[2].y);
            }
            return new Tuple2(geometry, new Rectangle2D.Double(xmin, ymin, xmax - xmin, ymax - ymin));
          } catch (Exception e) {
            // Return a rectangle that is outside the search space (the world)
            return new Tuple2(geometry, new Rectangle2D.Double(5000, 5000, 1, 1));
          }
        };
        refinement = pair -> {
          try {
            return ((org.locationtech.jts.geom.Geometry)pair._1).intersects((org.locationtech.jts.geom.Geometry) pair._2);
          } catch (Exception e) {
            return false;
          }
        };
        intersectionComputer = pair -> {
          try {
            return ((org.locationtech.jts.geom.Geometry) pair._1).intersection((org.locationtech.jts.geom.Geometry) pair._2);
          } catch (org.locationtech.jts.geom.TopologyException e) {
            return null;
          }
        };
        break;
      default:
        throw new RuntimeException("Unknown library " + library);
    }

    // Create SparkContext
    SparkConf conf = new SparkConf();
    if (!conf.contains("spark.master"))
      conf.setMaster("local[*]");
    System.out.printf("Using Spark master '%s'\n", conf.get("spark.master"));
    conf.setAppName("macrobenchmark");
    JavaSparkContext spark = new JavaSparkContext(conf);

    long start = System.nanoTime();

    String output = "output";
    FileSystem fs = new Path(output).getFileSystem(spark.hadoopConfiguration());
    if (fs.exists(new Path(output)))
      fs.delete(new Path(output), true);

    switch (operation) {
      case OP_RQ:
        JavaRDD<String> input = spark.textFile(filename1);
        JavaRDD<Object> features = input.map(parser).filter(emptyFilter);
        JavaRDD<Object> filteredFeatures = features.filter(rangeFilter);
        JavaRDD<String> result = filteredFeatures.map(wktizer);
        result.saveAsTextFile("output");
        break;
      case OP_SJ:
        final GridInfo grid = new GridInfo(-180, -90, 180, 90, 360, 180);
        // Read and grid partition the first input
        JavaRDD<Object> geometries1 = spark.textFile(filename1).map(parser).filter(emptyFilter);
        JavaRDD<Tuple2<Object, Rectangle2D.Double>> geometries1MBR = geometries1.map(mbrComputer);
        JavaPairRDD<Integer, Tuple2<Object, Rectangle2D.Double>> gridded1 = geometries1MBR.flatMapToPair(geomMBR -> {
          Rectangle2D.Double mbr = geomMBR._2;
          int[] cells = grid.getCells(mbr.getMinX(), mbr.getMinY(), mbr.getMaxX(), mbr.getMaxY());
          List<Tuple2<Integer, Tuple2<Object, Rectangle2D.Double>>> pairs = new ArrayList<>();
          for (int cell : cells) {
            pairs.add(new Tuple2<>(cell, geomMBR));
          }
          return pairs.iterator();
        }).repartition(144);

        // Read and grid partition the second input
        JavaRDD<Object> geometries2 = spark.textFile(filename2).map(parser).filter(emptyFilter);
        JavaRDD<Tuple2<Object, Rectangle2D.Double>> geometries2MBR = geometries2.map(mbrComputer);
        JavaPairRDD<Integer, Tuple2<Object, Rectangle2D.Double>> gridded2 = geometries2MBR.flatMapToPair(geomMBR -> {
          Rectangle2D.Double mbr = geomMBR._2;
          int[] cells = grid.getCells(mbr.getMinX(), mbr.getMinY(), mbr.getMaxX(), mbr.getMaxY());
          List<Tuple2<Integer, Tuple2<Object, Rectangle2D.Double>>> pairs = new ArrayList<>();
          for (int cell : cells) {
            pairs.add(new Tuple2<>(cell, geomMBR));
          }
          return pairs.iterator();
        }).repartition(144);

        // Co partition the two inputs
        JavaPairRDD<Integer, Tuple2<Iterable<Tuple2<Object, Rectangle2D.Double>>, Iterable<Tuple2<Object, Rectangle2D.Double>>>> grouped = gridded1.cogroup(gridded2);
        JavaRDD<Tuple2<Object, Object>> sjResults = grouped.flatMap(pair -> {
          // Extract geometries
          List<Tuple2<Object, Rectangle2D.Double>> geoms1 = new ArrayList<>();
          for (Tuple2<Object, Rectangle2D.Double> geom1 : pair._2._1)
            geoms1.add(geom1);
          List<Tuple2<Object, Rectangle2D.Double>> geoms2 = new ArrayList<>();
          for (Tuple2<Object, Rectangle2D.Double> geom2 : pair._2._2)
            geoms2.add(geom2);

          // Run planesweep algorithm
          List<Tuple2<Tuple2<Object, Rectangle2D.Double>, Tuple2<Object, Rectangle2D.Double>>> results = planeSweepRectangles(geoms1, geoms2);
          List<Tuple2<Object, Object>> filteredResults = new ArrayList<>();

          // Apply duplicate avoidance technique
          int cellID = pair._1;
          double[] cellMBR = grid.getCellMBR(cellID);
          for (int i = 0; i < results.size(); i++) {
            Tuple2<Tuple2<Object, Rectangle2D.Double>, Tuple2<Object, Rectangle2D.Double>> candidate = results.get(i);
            double refx = Math.max(candidate._1._2.getMinX(), candidate._2._2.getMinX());
            double refy = Math.max(candidate._1._2.getMinY(), candidate._2._2.getMinY());
            if (refx < cellMBR[0] || refx >= cellMBR[2] || refy < cellMBR[1] || refy >= cellMBR[3]) {
              results.remove(i);
            } else {
              // Add to results and advance the iterator
              filteredResults.add(new Tuple2(candidate._1._1, candidate._2._1));
              i++;
            }
          }

          // Return only the pairs of objects
          return filteredResults.iterator();
        });
        // Compute the intersection of each pair
        JavaRDD<Object> intersections = sjResults.filter(refinement).map(intersectionComputer).filter(emptyFilter);
        intersections.map(wktizer).saveAsTextFile(output);
        System.out.printf("Generated a total of %d polygons\n", intersections.count());
    }
    long end = System.nanoTime();
    System.out.printf("The operation %s finished in %f seconds\n",
        operation == Operation.OP_RQ? "rq" : "sj", (end-start)*1E-9);
    spark.stop();
  }

  /**
   * Computes the spatial join between two sets of rectangles and reports all overlapping pairs of indexes.
   *
   */
  public static List<Tuple2<Tuple2<Object, Rectangle2D.Double>, Tuple2<Object, Rectangle2D.Double>>> planeSweepRectangles(
      List<Tuple2<Object, Rectangle2D.Double>> geoms1, List<Tuple2<Object, Rectangle2D.Double>> geoms2) {
    geoms1.sort((a, b) -> (int) Math.signum(a._2.x - b._2.x));
    geoms2.sort((a, b) -> (int) Math.signum(a._2.x - b._2.x));
    List<Tuple2<Tuple2<Object, Rectangle2D.Double>, Tuple2<Object, Rectangle2D.Double>>> results = new ArrayList<>();
    long tstart = System.nanoTime();
    // Now, run the planesweep algorithm
    int i = 0, j = 0;
    while (i < geoms1.size() && j < geoms2.size()) {
      if (geoms1.get(i)._2.x < geoms2.get(j)._2.x) {
        // R1[i] is the left-most rectangle. Activate it and compare to all rectangles R2 until passing the right end
        // of R1[i]
        int jj = j;
        while (jj < geoms2.size() && geoms2.get(jj)._2.getMinX()  < geoms1.get(i)._2.getMaxX()) {
          // Compare the two rectangles R1[i] and R2[jj] and report if needed
          if (geoms1.get(i)._2.intersects(geoms2.get(jj)._2))
            // Found an overlap
            results.add(new Tuple2<>(geoms1.get(i), geoms2.get(jj)));
          jj++;
        }
        // Skip until the first record that might produce a result from i
        do {
          i++;
        } while (i < geoms1.size() && geoms1.get(i)._2.getMaxX() < geoms2.get(j)._2.getMinX());
      } else {
        // R2[j] is the left-most rectangle. Activate it and compare to all rectangles of R1 until passing the right
        // end of R2[j]
        int ii = i;
        while (ii < geoms1.size() && geoms1.get(ii)._2.getMinX()  < geoms2.get(j)._2.getMaxX()) {
          // Compare the two rectangles R1[i] and R2[jj] and report if needed
          if (geoms1.get(ii)._2.intersects(geoms2.get(j)._2))
            // Found an overlap
            results.add(new Tuple2<>(geoms1.get(ii), geoms2.get(j)));
          ii++;
        }
        // Skip until the first record that might produce a result from i
        do {
          j++;
        } while (j < geoms2.size() && geoms2.get(j)._2.getMaxX() < geoms1.get(i)._2.getMinX());

      }
    }
    long tend = System.nanoTime();
    System.out.printf("Finished planesweep between %d X %d rectangles and found %d results in %f seconds\n",
        geoms1.size(), geoms2.size(), results.size(), (tend - tstart) * 1E-9);
    return results;
  }

}
