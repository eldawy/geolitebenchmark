package edu.ucr.cs.bdlab.geolite;

import java.awt.Rectangle;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class GridInfo implements Serializable {


    public GridInfo(double x1, double y1, double x2, double y2, int nColumns, int nRows) {
        this.x1 = x1;
        this.y1 = y1;
        this.x2 = x2;
        this.y2 = y2;
        this.nColumns = nColumns;
        this.nRows = nRows;
    }
    double x1;
    double y1;
    double x2;
    double y2;
    int nColumns, nRows;

    public GridInfo() {}

    Rectangle overlaps(double xmin, double ymin, double xmax, double ymax) {
        int c1 = Math.max(0, (int) ((xmin - x1) * nColumns / (x2 - x1)));
        int c2 = Math.min(nColumns - 1, (int) ((xmax - x1) * nColumns / (x2 - x1)));
        int r1 = Math.max(0, (int) ((ymin - y1) * nRows / (y2 - y1)));
        int r2 = Math.min(nRows - 1, (int) ((ymax - y1) * nRows / (y2 - y1)));
        return new Rectangle(c1, r1, c2 - c1 + 1, r2 - r1 + 1);
    }

    int[] getCells(double xmin, double ymin, double xmax, double ymax) {
        Rectangle overlaps = overlaps(xmin, ymin, xmax, ymax);
        int[] cells = new int[overlaps.width * overlaps.height];
        int i = 0;
        for (int x = overlaps.x; x < overlaps.x + overlaps.width; x++) {
            for (int y = overlaps.y; y < overlaps.y + overlaps.height; y++) {
                cells[i++] = y * nColumns + x;
            }
        }
        return cells;
    }

    public double[] getCellMBR(int cellID) {
        int x = cellID % nColumns;
        int y = cellID / nColumns;
        double minx = x1 + x * (x2 - x1) / nColumns;
        double maxx = x1 + (x + 1) * (x2 - x1) / nColumns;
        double miny = y1 + y * (y2 - y1) / nRows;
        double maxy = y1 + (y + 1) * (y2 - y1) / nRows;
        return new double[] {minx, miny, maxx, maxy};
    }
}
