package edu.ucr.cs.bdlab.geolite;

import edu.ucr.cs.bdlab.geolite.twod.Polygon2D;
import edu.ucr.cs.bdlab.wkbparser.WKBParser;
import edu.ucr.cs.bdlab.wktparser.ParseException;
import edu.ucr.cs.bdlab.wktparser.WKTParser;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

public class GeoLiteProcessor implements LibraryProcessor {

  private boolean reuse;

  public GeoLiteProcessor(boolean reuse) {
    this.reuse = reuse;
  }

  @Override
  public void generatePolygons(int numGeometries, int numPointsPerGeometry, boolean discard) {
    assert !(!discard && reuse) : "Cannot reuse objects while not discarding them";
    int numGeneratedPolygons = 0;
    long start = System.nanoTime();
    Object[] geometries = discard? null : new Object[numGeometries];
    // Generate the polygons as desired
    try {
      Polygon2D polygon = null;
      if (reuse)
        polygon = new Polygon2D();
      for (numGeneratedPolygons = 0; numGeneratedPolygons < numGeometries; numGeneratedPolygons++) {
        // generate polygon
        if (reuse)
          polygon.setEmpty();
        else
          polygon = new Polygon2D();
        for (int i = 0; i < numPointsPerGeometry; i++) {
          polygon.addPoint(i, i);
        }
        if (!discard)
          geometries[numGeneratedPolygons] = polygon;
      }
    } catch (OutOfMemoryError e) {
      System.err.println("Out of memory");
      throw e;
    }
    long end = System.nanoTime();
    System.out.printf("The operation polygons finished in %f seconds\n", (end-start)*1E-9);
    if (!discard) {
      System.gc();
      System.out.printf("Total memory = %f MB, free memory = %f MB\n",
              Runtime.getRuntime().totalMemory()  / 1024.0 / 1024.0, Runtime.getRuntime().freeMemory() / 1024.0 / 1024.0);

      System.out.printf("Total memory usage is %f MB\n",
              (Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory()) / 1024.0 / 1024.0);
    }
  }

  @Override
  public void parseWKT(String filename, int maxNumGeometries, boolean discard) throws IOException, ParseException {
    assert !(!discard && reuse) : "Cannot reuse objects while not discarding them";
    WKTParser parser = new WKTParser();
    long numParsedGeometries = 0;
    long start = System.nanoTime();
    // Parse the geometries and store in an array only of discard is false
    BufferedReader input = new BufferedReader(new FileReader(filename));
    List<IGeometry> geometries = new ArrayList();
    long maxMemoryUsage = 0;
    try {
      String line;
      IGeometry geometry = null;
      while ((line = input.readLine()) != null && numParsedGeometries < maxNumGeometries) {
        try {
          if (!reuse)
            geometry = null;
          // Parse line from WKT
          geometry = parser.parse(line, geometry);
          if (!discard && geometry != null)
            geometries.add(geometry);
          numParsedGeometries++;
        } catch (Exception e) {
          // Skip over this error
        }
        maxMemoryUsage = Math.max(maxMemoryUsage, Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory());
      }
    } catch (OutOfMemoryError e) {
      System.err.println("Ran out of memory after processing "+numParsedGeometries+" geometries");
      throw e;
    } finally {
      input.close();
    }
    long end = System.nanoTime();
    System.out.printf("The operation wkt finished in %f seconds\n", (end-start)*1E-9);
    System.out.printf("The operation wkt terminated after processing %d records", numParsedGeometries);

    if (!discard) {
      System.out.printf("Total memory usage is %f MB\n", maxMemoryUsage / 1024.0 / 1024.0);

      start = System.nanoTime();
      // Calculate number of points per geometry
      for (IGeometry geometry : geometries) {
        if (!geometry.isEmpty())
          geometry.getNumPoints();
      }
      end = System.nanoTime();

      System.out.printf("The operation pointcount finished in %f seconds\n", (end-start)*1E-9);

      start = System.nanoTime();
      // Calculate the area of each geometry
      for (IGeometry geometry : geometries) {
        try {
          geometry.getVolume();
        } catch (Exception e) {}
      }
      end = System.nanoTime();

      System.out.printf("The operation area finished in %f seconds\n", (end-start)*1E-9);
    }
  }

  @Override
  public void parseWKB(byte[][] wkbData, boolean discard) {
    assert !(!discard && reuse) : "Cannot reuse objects while not discarding them";
    int numParsedGeometries = 0;
    long start = System.nanoTime();
    // Parse the geometries and store in an array only of discard is false
    try {
      IGeometry geometry = null;
      Object[] geometries = new Object[wkbData.length];
      for (numParsedGeometries = 0; numParsedGeometries < wkbData.length; numParsedGeometries++) {
        WKBParser parser = new WKBParser();
        if (!reuse)
          geometry = null;
        ByteBuffer wkb = ByteBuffer.wrap(wkbData[numParsedGeometries]);
        // Parse the geometry from wkb
        geometry = parser.parse(wkb, geometry);
        if (!discard)
          geometries[numParsedGeometries] = geometry;
      }
    } catch (OutOfMemoryError e) {
      // Out of memory error. Terminate at that point and report the data processed so far.
      throw e;
    }
    long end = System.nanoTime();
    System.out.printf("The operation wkb finished in %f seconds\n", (end-start)*1E-9);
    System.out.printf("The operation wkb terminated after processing %d records", numParsedGeometries);
  }
}
