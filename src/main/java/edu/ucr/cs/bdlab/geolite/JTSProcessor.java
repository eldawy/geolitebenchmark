package edu.ucr.cs.bdlab.geolite;

import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.geom.Polygon;
import org.locationtech.jts.geom.impl.PackedCoordinateSequenceFactory;
import org.locationtech.jts.io.ParseException;
import org.locationtech.jts.io.WKBReader;
import org.locationtech.jts.io.WKTReader;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class JTSProcessor implements LibraryProcessor {

  public enum CoordType {CT_ARRAY, CT_PACKEDDOUBLE, CT_PACKEDFLOAT}

  private CoordType coordType;

  private GeometryFactory factory;

  public JTSProcessor(CoordType type) {
    this.coordType = type;
    switch (type) {
      case CT_ARRAY:
        this.factory = new GeometryFactory();
        break;
      case CT_PACKEDDOUBLE:
        this.factory = new GeometryFactory(PackedCoordinateSequenceFactory.DOUBLE_FACTORY);
        break;
      case CT_PACKEDFLOAT:
        this.factory = new GeometryFactory(PackedCoordinateSequenceFactory.FLOAT_FACTORY);
        break;
      default:
        throw new RuntimeException("Unsupported type "+type);
    }
  }

  @Override
  public void generatePolygons(int numGeometries, int numPointsPerGeometry, boolean discard) {
    int numGeneratedPolygons = 0;
    long start = System.nanoTime();
    Object[] geometries = discard ? null : new Object[numGeometries];
    // Generate the polygons as desired
    long maxMemoryUsage = 0;
    try {
      for (numGeneratedPolygons = 0; numGeneratedPolygons < numGeometries; numGeneratedPolygons++) {
        // generate polygon
        Coordinate[] coordinates = new Coordinate[numPointsPerGeometry+1];
        for (int i = 0; i < numPointsPerGeometry; i++) {
          coordinates[i] = new Coordinate(i, i);
        }
        coordinates[numPointsPerGeometry] = coordinates[0];
        Polygon polygon = factory.createPolygon(coordinates);
        if (!discard)
          geometries[numGeneratedPolygons] = polygon;
        maxMemoryUsage = Math.max(maxMemoryUsage, Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory());
      }
    } catch (OutOfMemoryError e) {
      System.err.println("Ran out of memory");
      throw e;
    }
    long end = System.nanoTime();
    System.out.printf("The operation polygons finished in %f seconds\n", (end-start)*1E-9);
    System.out.printf("The operation polygons terminated after processing %d records\n", numGeneratedPolygons);
    if (!discard) {
      System.gc();
      System.out.printf("Total memory = %f MB, free memory = %f MB\n",
              Runtime.getRuntime().totalMemory()  / 1024.0 / 1024.0, Runtime.getRuntime().freeMemory() / 1024.0 / 1024.0);
      System.out.printf("Total memory usage is %f MB\n", maxMemoryUsage / 1024.0 / 1024.0);
    }
  }

  @Override
  public void parseWKT(String filename, int maxNumGeometries, boolean discard) throws IOException {
    WKTReader reader = new WKTReader(factory);
    long numParsedGeometries = 0;
    long start = System.nanoTime();
    // Parse the geometries and store in an array only of discard is false
    BufferedReader input = new BufferedReader(new FileReader(filename));
    List<Geometry> geometries = new ArrayList();
    try {
      String wkt;
      while ((wkt = input.readLine()) != null && numParsedGeometries < maxNumGeometries) {
        // parse line from WKT
        try {
          Geometry geometry = reader.read(wkt);
          if (geometry  != null && !discard)
            geometries.add(geometry);
          numParsedGeometries++;
        } catch (ParseException e) {
          System.err.println("Error parsing line #" + numParsedGeometries + " '" + wkt + "'");
        }
      }
    } catch (OutOfMemoryError e) {
      System.err.println("Ran out of memory after processing "+numParsedGeometries+" geometries");
      throw e;
    } finally {
      input.close();
    }
    long end = System.nanoTime();
    System.out.printf("The operation wkt finished in %f seconds\n", (end-start)*1E-9);
    System.out.printf("The operation wkt terminated after processing %d records\n", numParsedGeometries);

    if (!discard) {
      if (!discard) {
        System.gc();
        System.out.printf("Total memory = %f MB, free memory = %f MB\n",
                Runtime.getRuntime().totalMemory()  / 1024.0 / 1024.0, Runtime.getRuntime().freeMemory() / 1024.0 / 1024.0);
        System.out.printf("Total memory usage is %f MB\n",
                (Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory()) / 1024.0 / 1024.0);
      }

      start = System.nanoTime();
      // Calculate number of points per geometry
      long totalCount = 0;
      for (Geometry geometry : geometries) {
        int numPoints = geometry.getNumPoints();
        totalCount += numPoints;
      }
      end = System.nanoTime();

      System.out.printf("Total point count is %d\n", totalCount);
      System.out.printf("The operation pointcount finished in %f seconds\n", (end-start)*1E-9);

      start = System.nanoTime();
      double totalArea = 0.0;
      // Calculate the area of each geometry
      for (Geometry geometry : geometries) {
        double area = geometry.getArea();
        totalArea += area;
      }
      end = System.nanoTime();

      System.out.printf("Total area is %f\n", totalArea);
      System.out.printf("The operation area finished in %f seconds\n", (end-start)*1E-9);
    }
  }

  @Override
  public void parseWKB(byte[][] wkbData, boolean discard) {
    WKBReader reader = new WKBReader(factory);
    int numParsedGeometries = 0;
    long start = System.nanoTime();
    // Parse the geometries and store in an array only of discard is false
    try {
      Geometry[] geometries = discard? null : new Geometry[wkbData.length];
      for (numParsedGeometries = 0; numParsedGeometries < wkbData.length; numParsedGeometries++) {
        byte[] wkb = wkbData[numParsedGeometries];
        // parse the geometry from wkb
        try {
          Geometry geometry = reader.read(wkb);
          if (!discard)
            geometries[numParsedGeometries] = geometry;
        } catch (ParseException e) {
          System.err.println("Error parsing WKB #"+numParsedGeometries);
        }
      }
    } catch (OutOfMemoryError e) {
      // Out of memory error. Terminate at that point and report the data processed so far.
      System.err.println("Out of memory");
      throw e;
    }
    long end = System.nanoTime();
    System.out.printf("The operation wkb finished in %f seconds\n", (end-start)*1E-9);
    System.out.printf("The operation wkb terminated after processing %d records", numParsedGeometries);
  }

}
