package edu.ucr.cs.bdlab.geolite;

import edu.ucr.cs.bdlab.wktparser.ParseException;
import org.locationtech.jts.geom.CoordinateSequenceFactory;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.io.WKBWriter;
import org.locationtech.jts.io.WKTReader;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MicroBenchmarks {
  enum CGLibrary {CG_GEOS, CG_GEOLITE, CG_ESRI, CG_JTS}
  enum Operation {OP_NONE, OP_WKT, OP_WKB, OP_GENERATE, OP_POINT_COUNT, OP_AREA}

  public static void main(String[] args) throws IOException, ParseException {
    boolean discard = false;
    boolean reuse = false;
    int numGeometries = Integer.MAX_VALUE; // Virtually, no limit
    int numPointsPerGeometry = 50;
    CGLibrary library = CGLibrary.CG_GEOS;
    Operation operation = Operation.OP_NONE;
    JTSProcessor.CoordType coordType = JTSProcessor.CoordType.CT_ARRAY;
    String filename = null;

    if (args.length == 0) {
      System.err.println("Usage: main [inputfile] --operation=<op> [additional parameters]");
      System.err.println("operations {wkt, wkb, polygons, pointcount, area}");
      System.err.println("--discard (--no-discard) discard geometries directly after creation");
      System.err.println( "--reuse (--no-reuse) reuse objects only with geolite");
      System.err.println( "--num-geometries=<N> number of geometries to generate or maximum number of geometries to read");
      System.err.println( "--points-per-geometry=<n> number of points to generate per polygon");
      System.err.println( "--coord=<ct> type of coordinate sequence {array, packeddouble, packedfloat}");
      System.err.println( "--library=<lib> the CG library to use {geolite, jts, esri}");
      System.exit(1);
    }

    for (String arg : args) {
      if (arg.startsWith("--")) {
        // Begins with two dashes. Identify a parameter
        int equalSign = arg.indexOf("=");
        String name, value;
        if (equalSign != -1) {
          name = arg.substring(2, equalSign);
          value = arg.substring(equalSign + 1);
        } else {
          name = arg.substring(2);
          value = "";
        }
        switch (name) {
          case "discard": discard = true; break;
          case "no-discard": discard = false; break;
          case "reuse": reuse = true; break;
          case "no-reuse": reuse = false; break;
          case "num-geometries": numGeometries = Integer.parseInt(value); break;
          case "points-per-geometry": numPointsPerGeometry = Integer.parseInt(value); break;
          case "library":
            switch (value) {
              case "geolite": library = CGLibrary.CG_GEOLITE; break;
              case "esri": library = CGLibrary.CG_ESRI; break;
              case "jts": library = CGLibrary.CG_JTS; break;
              default: throw new RuntimeException("Unknown library "+value);
            }
            break;
          case "coord":
            switch (value) {
              case "array": coordType = JTSProcessor.CoordType.CT_ARRAY; break;
              case "packeddouble": coordType = JTSProcessor.CoordType.CT_PACKEDDOUBLE; break;
              case "packedfloat": coordType = JTSProcessor.CoordType.CT_PACKEDFLOAT; break;
              default: throw new RuntimeException("Unknown coordinate type "+value);
            }
            break;
          case "operation":
            switch (value) {
              case "wkt": operation = Operation.OP_WKT; break;
              case "wkb": operation = Operation.OP_WKB; break;
              case "polygons": operation = Operation.OP_GENERATE; break;
              case "pointcount": operation = Operation.OP_POINT_COUNT; break;
              case "area": operation = Operation.OP_AREA; break;
              default: throw new RuntimeException("Unknown operation "+value);
            }
        }
      } else {
        filename = arg;
      }
    }
    LibraryProcessor libraryProcessor;
    switch (library) {
      case CG_GEOLITE:
        libraryProcessor = new GeoLiteProcessor(reuse);
        break;
      case CG_ESRI:
        libraryProcessor = new EsriProcessor(reuse);
        break;
      case CG_JTS:
        libraryProcessor = new JTSProcessor(coordType);
        break;
      default:
        throw new RuntimeException("Unknown library " + library);
    }

    switch (operation) {
      case OP_GENERATE:
        libraryProcessor.generatePolygons(numGeometries, numPointsPerGeometry, discard);
        break;
      case OP_WKT:
        libraryProcessor.parseWKT(filename, numGeometries, discard);
        break;
      case OP_WKB:
        libraryProcessor.parseWKB(readWKB(filename, numGeometries), discard);
        break;
      case OP_AREA:
      case OP_POINT_COUNT:
        libraryProcessor.parseWKT(filename, numGeometries, false);
        break;
      default:
        throw new RuntimeException("Unsupported operation "+operation);
    }
  }

  public static byte[][] readWKB(String filename, int numGeometries) throws IOException {
    WKTReader reader = new WKTReader();
    WKBWriter writer = new WKBWriter();
    // Parse the geometries and store in an array only of discard is false
    BufferedReader input = new BufferedReader(new FileReader(filename));
    List<byte[]> wkbs = new ArrayList<>();
    int numParsedGeometries = 0;
    try {
      String wkt;
      while ((wkt = input.readLine()) != null && numParsedGeometries < numGeometries) {
        // parse line from WKT
        try {
          Geometry geometry = reader.read(wkt);
          if (geometry != null) {
            byte[] wkb = writer.write(geometry);
            wkbs.add(wkb);
            numParsedGeometries++;
          }
        } catch (org.locationtech.jts.io.ParseException e) {
          System.err.println("Error parsing line #" + numParsedGeometries + " '" + wkt + "'");
        }
      }
    } catch (OutOfMemoryError e) {
      System.err.println("Ran out of memory after processing "+numParsedGeometries+" geometries");
    } finally {
      input.close();
    }
    return wkbs.toArray(new byte[0][]);
  }

}
