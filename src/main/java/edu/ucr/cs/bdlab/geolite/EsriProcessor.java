package edu.ucr.cs.bdlab.geolite;

import com.esri.core.geometry.MultiPath;
import com.esri.core.geometry.Polygon;
import com.esri.core.geometry.ogc.OGCGeometry;

import java.io.*;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

public class EsriProcessor implements LibraryProcessor {

  private final boolean reuse;

  public EsriProcessor(boolean reuse) {
    this.reuse = reuse;
  }

  @Override
  public void generatePolygons(int numGeometries, int numPointsPerGeometry, boolean discard) {
    assert !(!discard && reuse) : "Cannot reuse objects while not discarding them";
    int numGeneratedPolygons = 0;
    long start = System.nanoTime();
    Object[] geometries = discard? null : new Object[numGeometries];
    long maxMemoryUsage = 0;
    // Generate the polygons as desired
    try {
      Polygon polygon = null;
      com.esri.core.geometry.Line segment = new com.esri.core.geometry.Line();
      if (reuse)
        polygon = new Polygon();
      for (numGeneratedPolygons = 0; numGeneratedPolygons < numGeometries; numGeneratedPolygons++) {
        // generate polygon
        if (reuse)
          polygon.setEmpty();
        else
          polygon = new Polygon();
        for (int i = 0; i < numPointsPerGeometry; i++) {
          segment.setStartXY(i, i);
          segment.setEndXY(i, i);
          polygon.addSegment(segment, false);
        }
        polygon.closePathWithLine();
        if (!discard)
          geometries[numGeneratedPolygons] = polygon;
        if (numGeneratedPolygons % 1000 == 0) {
          maxMemoryUsage = Math.max(maxMemoryUsage, Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory());
        }
      }
    } catch (OutOfMemoryError e) {
      System.err.println("Ran out of memory!");
      throw e;
    }
    long end = System.nanoTime();
    System.out.printf("The operation polygons finished in %f seconds\n", (end-start)*1E-9);
    System.out.printf("The operation polygons terminated after processing %d records\n", numGeneratedPolygons);
    if (!discard) {
      System.out.printf("Total memory usage is %f MB\n", maxMemoryUsage / 1024.0 / 1024.0);
      System.out.printf("Final memory usage is %f MB\n",
              (Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory()) / 1024.0 / 1024.0);
    }
  }

  @Override
  public void parseWKT(String filename, int maxNumGeometries, boolean discard) throws IOException {
    long numParsedGeometries = 0;
    long start = System.nanoTime();
    // Parse the geometries and store in an array only of discard is false
    BufferedReader input = new BufferedReader(new FileReader(filename));
    List<OGCGeometry> geometries = new ArrayList();
    long maxMemoryUsage = 0;
    try {
      String wkt;
      while ((wkt = input.readLine()) != null && numParsedGeometries < maxNumGeometries) {
        try {
          // Parse line from WKT
          OGCGeometry geom = OGCGeometry.fromText(wkt);
          if (!discard && geom != null)
            geometries.add(geom);
          numParsedGeometries++;
        } catch (Exception e) {

        }
        maxMemoryUsage = Math.max(maxMemoryUsage, Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory());
      }
    } catch (OutOfMemoryError e) {
      System.err.println("Ran out of memory after processing "+numParsedGeometries+" geometries");
      throw e;
    } finally {
      input.close();
    }
    long end = System.nanoTime();
    System.out.printf("The operation wkt finished in %f seconds\n", (end-start)*1E-9);
    System.out.printf("The operation wkt terminated after processing %d records", numParsedGeometries);

    if (!discard) {
      System.out.printf("Total memory usage is %f MB\n", maxMemoryUsage / 1024.0 / 1024.0);

      start = System.nanoTime();
      // Calculate number of points per geometry
      for (OGCGeometry geom : geometries) {
        int numPoints = 0;
        if (geom.getEsriGeometry() == null)
          continue;
        switch (geom.getEsriGeometry().getType()) {
          case Point: numPoints = 1; break;
          case Line: numPoints = 2; break;
          case Envelope: numPoints = 4; break;
          case Polyline: numPoints = ((MultiPath) geom.getEsriGeometry()).getPointCount(); break;
          default: numPoints = 0;
        }
      }
      end = System.nanoTime();

      System.out.printf("The operation pointcount finished in %f seconds\n", (end-start)*1E-9);

      start = System.nanoTime();
      // Calculate the area of each geometry
      for (OGCGeometry geom : geometries) {
        if (geom.getEsriGeometry() == null)
          continue;
        double area = geom.getEsriGeometry().calculateArea2D();
      }
      end = System.nanoTime();

      System.out.printf("The operation area finished in %f seconds\n", (end-start)*1E-9);
    }
  }

  @Override
  public void parseWKB(byte[][] wkbData, boolean discard) {
    int numParsedGeometries = 0;
    long start = System.nanoTime();
    // Parse the geometries and store in an array only of discard is false
    try {
      Object[] geometries = new Object[wkbData.length];
      for (numParsedGeometries = 0; numParsedGeometries < wkbData.length; numParsedGeometries++) {
        byte[] wkb = wkbData[numParsedGeometries];
        try {
          // parse the geometry from wkb
          OGCGeometry geometry = OGCGeometry.fromBinary(ByteBuffer.wrap(wkb));
          if (!discard)
            geometries[numParsedGeometries] = geometry;
        } catch (Exception e) {}
      }
    } catch (OutOfMemoryError e) {
      // Out of memory error. Terminate at that point and report the data processed so far.
      throw e;
    }
    long end = System.nanoTime();
    System.out.printf("The operation wkb finished in %f seconds\n", (end-start)*1E-9);
    System.out.printf("The operation wkb terminated after processing %d records", numParsedGeometries);
  }


}
