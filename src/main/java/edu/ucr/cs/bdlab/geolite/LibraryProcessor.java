package edu.ucr.cs.bdlab.geolite;

import edu.ucr.cs.bdlab.wktparser.ParseException;

import java.io.FileNotFoundException;
import java.io.IOException;

public interface LibraryProcessor {

  /**
   * Generate a list of random polygons.
   * @param numGeometries the number of geometries to generate.
   * @param numPointsPerGeometry the number of points per geometry.
   * @param discard if {@code true}, each geometry is discarded immediately after creation.
   */
  void generatePolygons(int numGeometries, int numPointsPerGeometry, boolean discard);

  /**
   * Parse up-to numGeometries from the given file as WKT. If discard is set to false, this method should follow by
   * calculating the number of points per geometry and the area of each geometry. This method should also print
   * to stdout the time taken for parsing, calculating the number of points, and calculating the area; each of
   * these times should be calculated and printed separately.
   * @param filename the filename that contains the data
   * @param maxNumGeometries upper bound on the number of geometries to read
   * @param discard discard each geometry after creation. Do not keep in memory or in a list.
   */
  void parseWKT(String filename, int maxNumGeometries, boolean discard) throws IOException, ParseException;

  /**
   * Parse all the given WKB geometries.
   * @param wkbData a two dimensional array where the first index indicates number of geometries and the second
   *                index represent the byte within that geometry.
   */
  void parseWKB(byte[][] wkbData, boolean discard);
}
