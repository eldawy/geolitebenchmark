package edu.ucr.cs.bdlab.geolite;

import com.esri.core.geometry.Geometry;
import com.esri.core.geometry.GeometryCursor;
import com.esri.core.geometry.OGCStructure;
import com.esri.core.geometry.SpatialReference;
import com.esri.core.geometry.ogc.OGCConcreteGeometryCollection;
import com.esri.core.geometry.ogc.OGCGeometry;
import com.esri.core.geometry.ogc.OGCGeometryCollection;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.nio.ByteBuffer;
import java.util.ArrayList;

/**
 * A wrapper around OGCGeometry to make it serializable.
 */
public class OGCGeometryCollectionSerializable extends OGCConcreteGeometryCollection implements Externalizable {

  public OGCGeometryCollectionSerializable() {
    super(new ArrayList<>(), SpatialReference.create(3426));
  }

  public OGCGeometryCollectionSerializable(OGCGeometryCollection geometry) {
    super(geometry, geometry.esriSR);
  }

  @Override
  public int dimension() {
    return internal.dimension();
  }

  @Override
  public int coordinateDimension() {
    return internal.coordinateDimension();
  }

  @Override
  public String geometryType() {
    return internal.geometryType();
  }

  @Override
  public long estimateMemorySize() {
    return internal.estimateMemorySize();
  }

  @Override
  public int SRID() {
    return internal.SRID();
  }

  @Override
  public OGCGeometry envelope() {
    return internal.envelope();
  }

  @Override
  public String asText() {
    return internal.asText();
  }

  @Override
  public ByteBuffer asBinary() {
    return internal.asBinary();
  }

  @Override
  public String asGeoJson() {
    return internal.asGeoJson();
  }

  @Override
  public String asJson() {
    return internal.asJson();
  }

  @Override
  public boolean isEmpty() {
    return internal == null || internal.isEmpty();
  }

  @Override
  public double MinZ() {
    return internal.MinZ();
  }

  @Override
  public double MaxZ() {
    return internal.MaxZ();
  }

  @Override
  public double MinMeasure() {
    return internal.MinMeasure();
  }

  @Override
  public double MaxMeasure() {
    return internal.MaxMeasure();
  }

  @Override
  public boolean isSimple() {
    return internal.isSimple();
  }

  @Override
  public boolean isSimpleRelaxed() {
    return internal.isSimpleRelaxed();
  }

  @Override
  @Deprecated
  public OGCGeometry MakeSimpleRelaxed(boolean forceProcessing) {
    return internal.MakeSimpleRelaxed(forceProcessing);
  }

  @Override
  public OGCGeometry makeSimpleRelaxed(boolean forceProcessing) {
    return internal.makeSimpleRelaxed(forceProcessing);
  }

  @Override
  public OGCGeometry makeSimple() {
    return internal.makeSimple();
  }

  @Override
  public boolean is3D() {
    return internal.is3D();
  }

  @Override
  public boolean isMeasured() {
    return internal.isMeasured();
  }

  @Override
  public OGCGeometry boundary() {
    return internal.boundary();
  }

  @Override
  public boolean Equals(OGCGeometry another) {
    return internal.Equals(another);
  }

  @Override
  @Deprecated
  public boolean equals(OGCGeometry another) {
    return internal.equals(another);
  }

  @Override
  public boolean disjoint(OGCGeometry another) {
    return internal.disjoint(another);
  }

  @Override
  public boolean intersects(OGCGeometry another) {
    return internal.intersects(another);
  }

  @Override
  public boolean touches(OGCGeometry another) {
    return internal.touches(another);
  }

  @Override
  public boolean crosses(OGCGeometry another) {
    return internal.crosses(another);
  }

  @Override
  public boolean within(OGCGeometry another) {
    return internal.within(another);
  }

  @Override
  public boolean contains(OGCGeometry another) {
    return internal.contains(another);
  }

  @Override
  public boolean overlaps(OGCGeometry another) {
    return internal.overlaps(another);
  }

  @Override
  public boolean relate(OGCGeometry another, String matrix) {
    return internal.relate(another, matrix);
  }

  @Override
  public OGCGeometry locateAlong(double mValue) {
    return internal.locateAlong(mValue);
  }

  @Override
  public OGCGeometry locateBetween(double mStart, double mEnd) {
    return internal.locateBetween(mStart, mEnd);
  }

  @Override
  public double distance(OGCGeometry another) {
    return internal.distance(another);
  }

  @Override
  public OGCGeometry buffer(double distance) {
    return internal.buffer(distance);
  }

  @Override
  public OGCGeometry centroid() {
    return internal.centroid();
  }

  @Override
  public OGCGeometry convexHull() {
    return internal.convexHull();
  }

  @Override
  public OGCGeometry intersection(OGCGeometry another) {
    return internal.intersection(another);
  }

  @Override
  public OGCGeometry union(OGCGeometry another) {
    return internal.union(another);
  }

  @Override
  public OGCGeometry difference(OGCGeometry another) {
    return internal.difference(another);
  }

  @Override
  public OGCGeometry symDifference(OGCGeometry another) {
    return internal.symDifference(another);
  }

  @Override
  public Geometry getEsriGeometry() {
    return internal.getEsriGeometry();
  }

  @Override
  public GeometryCursor getEsriGeometryCursor() {
    return internal.getEsriGeometryCursor();
  }

  @Override
  public SpatialReference getEsriSpatialReference() {
    return internal.getEsriSpatialReference();
  }

  public static OGCGeometry createFromEsriCursor(GeometryCursor gc, SpatialReference sr) {
    return OGCGeometry.createFromEsriCursor(gc, sr);
  }

  public static OGCGeometry createFromEsriCursor(GeometryCursor gc, SpatialReference sr, boolean skipEmpty) {
    return OGCGeometry.createFromEsriCursor(gc, sr, skipEmpty);
  }

  public static OGCGeometry fromText(String text) {
    return OGCGeometry.fromText(text);
  }

  public static OGCGeometry fromBinary(ByteBuffer binary) {
    return OGCGeometry.fromBinary(binary);
  }

  public static OGCGeometry fromEsriShape(ByteBuffer buffer) {
    return OGCGeometry.fromEsriShape(buffer);
  }

  public static OGCGeometry fromJson(String string) {
    return OGCGeometry.fromJson(string);
  }

  public static OGCGeometry fromGeoJson(String string) {
    return OGCGeometry.fromGeoJson(string);
  }

  public static OGCGeometry createFromEsriGeometry(Geometry geom, SpatialReference sr) {
    return OGCGeometry.createFromEsriGeometry(geom, sr);
  }

  public static OGCGeometry createFromEsriGeometry(Geometry geom, SpatialReference sr, boolean multiType) {
    return OGCGeometry.createFromEsriGeometry(geom, sr, multiType);
  }

  public static OGCGeometry createFromOGCStructure(OGCStructure ogcStructure, SpatialReference sr) {
    return OGCGeometry.createFromOGCStructure(ogcStructure, sr);
  }

  @Override
  protected boolean isConcreteGeometryCollection() {
    return true;
  }

  @Override
  public void setSpatialReference(SpatialReference esriSR_) {
    internal.setSpatialReference(esriSR_);
  }

  @Override
  public OGCGeometry convertToMulti() {
    return internal.convertToMulti();
  }

  @Override
  public OGCGeometry reduceFromMulti() {
    return internal.reduceFromMulti();
  }

  @Override
  public String toString() {
    return internal.toString();
  }

  @Override
  public boolean equals(Object other) {
    return internal.equals(other);
  }

  @Override
  public int hashCode() {
    return internal.hashCode();
  }

  OGCGeometry internal;

  @Override
  public void writeExternal(ObjectOutput out) throws IOException {
    ByteBuffer bb = this.internal.asBinary();
    out.writeInt(bb.limit());
    out.write(bb.array(), 0, bb.limit());
  }

  @Override
  public void readExternal(ObjectInput in) throws IOException {
    int length = in.readInt();
    byte[] bytes = new byte[length];
    in.readFully(bytes);
    this.internal = OGCGeometry.fromBinary(ByteBuffer.wrap(bytes));
  }
}
