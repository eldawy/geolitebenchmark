#include "geolite_polygon.h"

void Polygon::addPoint(double x, double y) {
    Point point(x, y);
    points.push_back(point);
}

void Polygon::setEmpty() {
    points.clear();
}