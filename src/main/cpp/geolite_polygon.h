#include<vector>

typedef std::pair<double, double> Point;
class Polygon {
    std::vector<Point> points;
public:
    void addPoint(double x, double y);
    void setEmpty();
};