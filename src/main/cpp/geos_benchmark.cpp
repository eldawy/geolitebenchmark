#include <geos/profiler.h>
#include <geos_c.h>

#include "geolite_polygon.h"
#include <fstream>
#include <iostream>
#include <sstream>
#include <chrono>

#include "stdlib.h"
#include "stdio.h"
#include "string.h"

enum CGLibrary {CG_GEOS, CG_GEOLITE};
enum Operation {OP_NONE, OP_WKT, OP_WKB, OP_GENERATE, OP_POINT_COUNT, OP_AREA};

int parseLine(char* line){
    // This assumes that a digit will be found and the line ends in " Kb".
    int i = strlen(line);
    const char* p = line;
    while (*p <'0' || *p > '9') p++;
    line[i-3] = '\0';
    i = atoi(p);
    return i;
}

int getPhysicalMemoryUsage(){ //Note: this value is in KB!
    // https://stackoverflow.com/questions/63166/how-to-determine-cpu-and-memory-consumption-from-inside-a-process
    FILE* file = fopen("/proc/self/status", "r");
    int result = -1;
    if (!file)
        return result;
    char line[128];

    while (fgets(line, 128, file) != NULL){
        if (strncmp(line, "VmRSS:", 6) == 0){
            result = parseLine(line);
            break;
        }
    }
    fclose(file);
    return result;
}


void geosWKTTest(std::string filename, unsigned long numGeometries, bool discard) {
    std::cout << "Parsing up-to " << numGeometries << " geometries from " << filename
              << " with " << (discard ? "discard" : "no discard") << std::endl;
    auto start = std::chrono::high_resolution_clock::now();
    
    std::vector<GEOSGeometry*> geoms;

    std::ifstream f(filename);
    std::string line;
    long i = 0;
    while(std::getline(f, line) && i < numGeometries) {
        GEOSGeometry* geometry = GEOSGeomFromWKT(line.c_str());
        if (!geometry) {
            std::cerr << "Error parsing line #"<< i << " in file " << filename << std::endl;
            std::cerr << line << std::endl;
        } else {
            if (!discard)
                geoms.push_back(geometry);
            else
                GEOSGeom_destroy(geometry);
            i++;
        }
    }
    f.close();

    auto end = std::chrono::high_resolution_clock::now(); 
    auto duration = std::chrono::duration_cast<std::chrono::microseconds>(end - start).count() * 1E-6;

    std::cout << "The operation wkt finished in " << duration << " seconds" << std::endl;

    if (!discard) {
        // Print memory usage
        int memKB = getPhysicalMemoryUsage();
        std::cout << "Total memory usage is "<< (memKB / 1024.0) <<" MB" << std::endl;
        // Now, calculate the area for all polygons and measure the time
        start = std::chrono::high_resolution_clock::now();
        // Compute the area for each geometry
        double totalArea = 0.0;
        for (const auto& g : geoms) {
            double area;
            GEOSArea(g, &area);
            totalArea += area;
        }
        end = std::chrono::high_resolution_clock::now(); 
        duration = std::chrono::duration_cast<std::chrono::microseconds>(end - start).count() * 1E-6;

        std::cout << "Total area of " << numGeometries
                          << " geometries in file " << filename << " is " << totalArea << std::endl;
        std::cout << "The operation area finished in " << duration << " seconds" << std::endl;

        // Now, calcluate number of points and measure the time
        start = std::chrono::high_resolution_clock::now();
        // Compute the area for each geometry
        long totalPointCount = 0;
        for (const auto& g : geoms) {
            totalPointCount += GEOSGeomGetNumPoints(g);
        }
        end = std::chrono::high_resolution_clock::now(); 
        duration = std::chrono::duration_cast<std::chrono::microseconds>(end - start).count() * 1E-6;

        std::cout << "Total number of points of " << numGeometries
                  << " geometries in file " << filename << " is " << totalPointCount << std::endl;
        std::cout << "The operation pointcount finished in " << duration << " seconds" << std::endl;
    }
    for (auto& g : geoms) {
        GEOSGeom_destroy(g);
    }
}

void geosWKBTest(std::string filename, unsigned long numGeometries) {
    // First, parse all geometries and convert them to WKB without measuring the time
    GEOSWKBWriter* wkbWriter = GEOSWKBWriter_create();
    std::vector<unsigned char*> wkbs;
    std::vector<size_t> wkbSizes;

    std::ifstream f(filename);
    std::string line;
    long i = 0;
    while(std::getline(f, line) && i < numGeometries) {
        GEOSGeometry* geometry = GEOSGeomFromWKT(line.c_str());
        if (!geometry) {
            std::cerr << "Error parsing line #"<< i << " in file " << filename << std::endl;
            std::cerr << line << std::endl;
        } else {
            size_t size;
            unsigned char* wkb = GEOSWKBWriter_write(wkbWriter, geometry, &size);
            wkbs.push_back(wkb);
            wkbSizes.push_back(size);
            GEOSGeom_destroy(geometry);
            i++;
        }
    }
    f.close();
    GEOSWKBWriter_destroy(wkbWriter);

    // Now, parse all WKBs back to geometries

    GEOSWKBReader* wkbReader = GEOSWKBReader_create();

    auto start = std::chrono::high_resolution_clock::now();

    for (int j = 0; j < wkbs.size(); j++) {
        unsigned char* wkb = wkbs[j];
        size_t wkbSize = wkbSizes[j];
        GEOSGeometry* geometry = GEOSWKBReader_read(wkbReader, wkb, wkbSize);
        GEOSGeom_destroy(geometry);
    }

    auto end = std::chrono::high_resolution_clock::now();
    auto duration = std::chrono::duration_cast<std::chrono::microseconds>(end - start).count() * 1E-6;

    std::cout << "The operation wkb finished in " << duration << " seconds" << std::endl;

    for (auto wkb : wkbs)
        delete wkb;
}

void geoliteGenerate(unsigned long numGeometries, int numPointsPerGeometry, bool discard, bool reuse) {
    std::cout << "Generating " << numGeometries << " geometries with " << numPointsPerGeometry << " points each"
            << " with " << (discard ? "discard" : "no discard")
            << " with " << (reuse ? "reuse (mutable)" : "no reuse (immutable)") << std::endl;
    if (!discard && reuse) {
        std::cerr << "Cannot keep the objects and reuse at the same time" << std::endl;
        return;
    }
    std::vector<Polygon*> polygons;
    auto start = std::chrono::high_resolution_clock::now();
    Polygon* p;
    if (reuse)
        p = new Polygon();
    for (int i = 0; i < numGeometries; i++) {
        if (!reuse)
            p = new Polygon();
        for (int x = 0; x < numPointsPerGeometry; x++) {
            p->addPoint(x, x);
        }
        if (!discard) {
            polygons.push_back(p);
        } else {
            if (reuse)
                p->setEmpty();
            else
                delete p;
        }
    }

    auto end = std::chrono::high_resolution_clock::now(); 
    auto duration = std::chrono::duration_cast<std::chrono::microseconds>(end - start).count() * 1E-6;

    std::cout << "The operation polygons finished in " << duration << " seconds" << std::endl;

    if (!discard) {
        // Print memory usage
        int memKB = getPhysicalMemoryUsage();
        std::cout << "Total memory usage is "<< (memKB / 1024.0) <<" MB" << std::endl;
    }
    for (auto poly : polygons) {
        delete poly;
    }
}

/**
 * Generate polygons using the GEOS library
 */
void geosGenerate(unsigned long numGeometries, int numPointsPerGeometry, bool discard) {
    std::cout << "Generating " << numGeometries << " geometries with " << numPointsPerGeometry << " points each"
            << " with " << (discard ? "discard" : "no discard") << std::endl;
    std::vector<GEOSGeometry*> polygons;
    auto start = std::chrono::high_resolution_clock::now();
    for (int i = 0; i < numGeometries; i++) {
        GEOSCoordSequence* coordSeq = GEOSCoordSeq_create(numPointsPerGeometry + 1, 2);
        for (int x = 0; x < numPointsPerGeometry; x++) {
            GEOSCoordSeq_setXY(coordSeq, x, x, x);
        }
        GEOSGeometry* ring = GEOSGeom_createLinearRing(coordSeq);
        GEOSGeometry* polygon = GEOSGeom_createPolygon(ring, NULL, 0);
        if (discard)
            GEOSGeom_destroy(polygon);
        else
            polygons.push_back(polygon);
    }

    auto end = std::chrono::high_resolution_clock::now(); 
    auto duration = std::chrono::duration_cast<std::chrono::microseconds>(end - start).count() * 1E-6;

    std::cout << "The operation polygons finished in " << duration << " seconds" << std::endl;

    if (!discard) {
        // Print memory usage
        int memKB = getPhysicalMemoryUsage();
        std::cout << "Total memory usage is "<< (memKB / 1024.0) <<" MB" << std::endl;
    }

    for (auto& p : polygons) {
        GEOSGeom_destroy(p);
    }
}

/**
 * Micro-benchmarks for the GEOS library. Part of the paper (Under preparation)
 * Yaming Zhang and Ahmed Eldawy
 */
int main(int argc, char** argv) {
    bool discard = false;
    bool reuse = false;
    unsigned long numGeometries = -1; // Virtually, no limit
    int numPointsPerGeometry;
    CGLibrary library = CG_GEOS;
    Operation operation = OP_NONE;
    std::string filename;

    if (argc == 1) {
        std::cerr << "Usage: " << argv[0] << " [inputfile] --operation=<op> [additional parameters]" << std::endl
                  << "operations {wkt, wkb, polygons, pointcount, area}" << std::endl
                  << "--discard (--no-discard) discard geometries directly after creation" << std::endl
                  << "--reuse (--no-reuse) reuse objects only with geolite" << std::endl
                  << "--num-geometries=<N> number of geometries to generate or maximum number of geometries to read" << std::endl
                  << "--points-per-geometry=<n> number of points to generate per polygon" << std::endl
                  << "--library=<lib> the CG library to use {geos, geolite}" << std::endl;
        exit(1);
    }

    for (int iarg = 1; iarg < argc; iarg++) {
        std::string arg = argv[iarg];
        if (arg.find("--") == 0) {
            // Begins with two dashes. Identify a parameter
            int equalSign = arg.find("=");
            std::string name, value;
            if (equalSign != std::string::npos) {
                name = arg.substr(2, equalSign - 2);
                value = arg.substr(equalSign + 1, std::string::npos);
            } else {
                name = arg.substr(2, std::string::npos);
                value = "";
            }
            if (name.compare("discard") == 0) {
                discard = true;
            } else if (name.compare("no-discard") == 0) {
                discard = false;
            } else if (name.compare("reuse") == 0) {
                reuse = true;
            } else if (name.compare("no-reuse") == 0) {
                reuse = false;
            } else if (name.compare("num-geometries") == 0) {
                numGeometries = std::stol(value);
            } else if (name.compare("points-per-geometry") == 0) {
                numPointsPerGeometry = std::stoi(value);
            } else if (name.compare("library") == 0) {
                if (value.compare("geos") == 0)
                    library = CG_GEOS;
                else if (value.compare("geolite") == 0)
                    library = CG_GEOLITE;
            } else if (name.compare("operation") == 0) {
                if (value.compare("wkt") == 0)
                    operation = OP_WKT;
                else if (value.compare("wkb") == 0)
                    operation = OP_WKB;
                else if (value.compare("polygons") == 0)
                    operation = OP_GENERATE;
                else if (value.compare("pointcount") == 0)
                    operation = OP_POINT_COUNT;
                else if (value.compare("area") == 0)
                    operation = OP_AREA;
            }
        } else {
            filename = arg;
        }
    }

    switch (library)
    {
    case CG_GEOS:
        initGEOS(nullptr, nullptr);
        switch (operation) {
        case OP_GENERATE:
            geosGenerate(numGeometries, numPointsPerGeometry, discard);
            break;
        case OP_WKT:
            geosWKTTest(filename, numGeometries, discard);
            break;
        case OP_WKB:
            geosWKBTest(filename, numGeometries);
            break;
        case OP_AREA:
        case OP_POINT_COUNT:
            geosWKTTest(filename, numGeometries, false);
            break;
        default: 
            std::cerr << "Unsupported operation " << operation << " in GEOS" << std::endl;
            exit(1);
        }
        finishGEOS();
        break;
    case CG_GEOLITE:
        switch (operation) {
        case OP_GENERATE:
            geoliteGenerate(numGeometries, numPointsPerGeometry, discard, reuse);
            break;
        default: 
            std::cerr << "Unsupported operation " << operation << " in GeoLite" << std::endl;
            exit(1);
        }
        break;
    default:
        std::cerr << "Unsupported library " << library << std::endl;
        exit(1);
    }
}