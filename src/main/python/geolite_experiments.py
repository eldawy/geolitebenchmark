#!/usr/bin/python3
import os
from datetime import datetime
import re # Regular expressions
import sys # For stdin and stderr
import os.path
from os import path
import subprocess
import time
import argparse

wkt_file = "all_objects.wkt"
join1_file = "parks_poly.wkt"
join2_file = "lakes_poly.wkt"

# List all attributes that will be written to the file for each operation
attributesPerOperation = {
  "wkt" : ["library", "jre", "discard", "reuse", "input", "num-geometries", "coord", "running-time", "total-memory"],
  "wkb" :["library", "jre", "discard", "reuse", "input", "num-geometries", "coord", "running-time"],
  "polygons": ["library",	"jre", "discard", "reuse", "num-geometries", "points-per-geometry", "coord",
               "running-time", "total-memory"],
  "pointcount": ["library",	"jre", "discard", "reuse", "num-geometries", "coord", "running-time"],
  "area": ["library",	"jre", "discard", "reuse", "num-geometries", "coord", "running-time"],
  "rq": ["library", "input", "coord", "running-time"],
  "sj": ["library", "input1", "input2", "coord", "running-time"]
}

# Runs the given command and returns its stdout
def runCmd(cmd):
  sys.stderr.write(f'{datetime.now()} => {cmd}\n')
  sys.stderr.flush()

  if dryrun:
    return ""
  p = subprocess.Popen(cmd, stdout=subprocess.PIPE)
  start_time = datetime.now()
  while p.poll() is None:
    time.sleep(5)
    if (datetime.now() - start_time).seconds > 1800:
      sys.stderr.write("Process timed out. killing ...\n")
      p.kill()

  if p.poll() != 0:
    sys.stderr.write("Process failed\n")
    sys.stderr.flush()

  (stdout, stderr) = p.communicate()
  #result = "The operation op finished in 33.4 seconds"
  return stdout.decode()

def extractRunningTime(result):
  pattern = re.compile(r'The operation ([\w]+) finished in (.*) seconds')
  return re.findall(pattern, result)

def extractNumProcessedGeometries(result):
  pattern = re.compile(r'The operation ([\w]+) terminated after processing (.*) records')
  return re.findall(pattern, result)

def extractTotalMemory(result):
  pattern = re.compile(r'Total memory usage is (.*) MB')
  return re.findall(pattern, result)

def runExperiment(parameters):
  if (experimentExists(parameters)):
    print("xxx Skipping experiment "+str(parameters))
    return
  command = []
  r = re.compile(r'geolite.*uber.*jar')
  jar_file = list(filter(r.match, os.listdir("target")))[0]
  if (parameters["library"] == "geos"):
    command.append("target/cpp/a.out")
  elif parameters["operation"] == "rq" or parameters["operation"] == "sj":
    command.append("spark-submit")
    command.append("--class")
    command.append("edu.ucr.cs.bdlab.geolite.MacroBenchmarks")
    command.append(f"target/{jar_file}")
  else:
    command.append("java")
    command.append("-cp")
    command.append(f"target/{jar_file}")
    command.append("-Xmx50g")
    command.append("-Xms50g")
    command.append("edu.ucr.cs.bdlab.geolite.MicroBenchmarks")
  for name in parameters:
    value = parameters[name]
    if name.startswith("input"):
      command.append(value)
    elif (isinstance(value, bool)):
      command.append(f"--{name}" if value else f"--no-{name}")
    else:
      command.append(f"--{name}={value}")
  result = runCmd(command)

  # Extract number of processed geometries. There is at most one
  numProcessedGeometries = list(extractNumProcessedGeometries(result))
  ng = None
  for (operation, numGeometries) in numProcessedGeometries:
    ng = numGeometries
    # experiment_result = parameters.copy()
    # experiment_result["operation"] = operation
    # experiment_result["num-processed-geometries"] = numGeometries
    # printResult(experiment_result)

  totalMemory = None
  for (x) in extractTotalMemory(result):
    totalMemory = x

  runningTimes = extractRunningTime(result)
  for (operation, time) in runningTimes:
    experiment_result = parameters.copy()
    experiment_result["operation"] = operation
    experiment_result["running-time"] = time
    if (operation == "polygons" or operation == "wkt") and ng is not None:
      experiment_result["num-processed-geometries"] = ng
    if (operation == "polygons" or operation == "wkt") and totalMemory is not None:
      experiment_result["total-memory"] = totalMemory
    printResult(experiment_result)

# Checks if the given experiment already exists on disk
def experimentExists(experiment):
  operation = experiment["operation"]
  attributeNames = attributesPerOperation[operation]
  filename = f"results_{operation}.csv"
  if not path.exists(filename):
    return False
  file = open(filename, "r")
  lines = file.read().split("\n")
  file.close
  if (lines[0] != "\t".join(attributeNames)):
    raise Exception(f"header file does not match {lines[0]} != {str(attributeNames)} in file {filename}")
  def getValue(attributeName):
    return str(experiment[attributeName]) if (attributeName in experiment) else None
  expected_line = list(map(getValue, attributeNames))
  for actual_line in lines:
    parts = actual_line.split("\t")
    match = True
    for i in range(0, len(expected_line)):
      match = match and (expected_line[i] is None or expected_line[i] == parts[i])
    if match:
      return True
  return False

def printResult(experiment):
  operation = experiment["operation"]
  attributeNames = attributesPerOperation[operation]
  filename = f"results_{operation}.csv"
  if not path.exists(filename):
    # First time, create the file
    file = open(filename, "w")
    file.write("\t".join(attributeNames))
    file.write("\n")
  else:
    file = open(filename, "a")

  def getValue(attributeName):
    return str(experiment[attributeName]) if (attributeName in experiment) else ""
  attributeValues = list(map(getValue, attributeNames))
  file.write("\t".join(attributeValues))
  file.write("\n")
  file.close

if __name__ == "__main__":
  parser = argparse.ArgumentParser(description='Run Geolite Experiments.')
  parser.add_argument('--dryrun', dest='dryrun', action='store_true',
                      help='Do not run the programs. Just list which programs would have run.')
  args = parser.parse_args()
  dryrun = args.dryrun

  allExperiments = []
  # The possible parameters are listed below. All case-sensitive
  # library: {geos, jts, esri, geolite}
  # operation: {wkt, wkb, polygons}
  # num-geometries: for limiting the number of geometries to read or the number of polygons to generate
  # points-per-geometry: number of points per geometry to generate
  # discard: Do not keep geometries in memory
  # coord: (Only for JTS) {array, packeddouble, packedfloat}
  # reuse: (Only for GeoLite and Esri)
  # input: The input filename for WKT and WKB operations
  # memory: Upper limit on memory in megabytes

  coordTypes = ["array", "packeddouble", "packedfloat"]
  libraries = ["geos", "jts", "esri", "geolite"]
  operations = ["polygons", "wkt", "wkb", "rq", "sj"]

  # 1- Test polygon creation
  # 1.1- Create polygons (operation:polygons}
  num_polygons = [1000, 10000, 100000, 1000000, 5000000]
  pointsPerPolygon = [50, 100, 500, 1000]

  for operation in operations:
    if operation == "polygons":
      for n in num_polygons:
        for library in libraries:
          if library == "geos":
            allExperiments.append({"library": library, "num-geometries": n, "points-per-geometry": 50, "operation": "polygons", "discard": True})
            allExperiments.append({"library": library, "num-geometries": n, "points-per-geometry": 50, "operation": "polygons", "discard": False})
          elif library == "geolite":
            allExperiments.append({"library": library, "num-geometries": n, "points-per-geometry": 50, "operation": "polygons", "reuse": False, "discard": False})
            allExperiments.append({"library": library, "num-geometries": n, "points-per-geometry": 50, "operation": "polygons", "reuse": False, "discard": True})
            allExperiments.append({"library": library, "num-geometries": n, "points-per-geometry": 50, "operation": "polygons", "reuse": True, "discard": True})
          elif library == "esri":
            allExperiments.append({"library": library, "num-geometries": n, "points-per-geometry": 50, "operation": "polygons", "reuse": False, "discard": False})
            allExperiments.append({"library": library, "num-geometries": n, "points-per-geometry": 50, "operation": "polygons", "reuse": False, "discard": True})
            allExperiments.append({"library": library, "num-geometries": n, "points-per-geometry": 50, "operation": "polygons", "reuse": True, "discard": True})
          elif library == "jts":
            for coordType in coordTypes:
              # Try with and without discard
              allExperiments.append({"library": "jts", "num-geometries": n, "points-per-geometry": 50, "operation": "polygons", "discard": False, "coord": coordType})
              allExperiments.append({"library": "jts", "num-geometries": n, "points-per-geometry": 50, "operation": "polygons", "discard": True, "coord": coordType})

      # 1.3- Create polygons while varying number of points per polygon
      numGeometries = 1000000
      #pointsPerPolygon = [1000] # TODO Comment later
      # numGeometries = 1000 # TODO Comment later
      for n in pointsPerPolygon:
        for library in libraries:
          if library == "geos":
            allExperiments.append({"library": library, "num-geometries": numGeometries, "points-per-geometry": n, "operation": "polygons", "discard": False})
            allExperiments.append({"library": library, "num-geometries": numGeometries, "points-per-geometry": n, "operation": "polygons", "discard": True})
          elif library == "geolite":
            allExperiments.append({"library": library, "num-geometries": numGeometries, "points-per-geometry": n, "operation": "polygons", "reuse": False, "discard": False})
            allExperiments.append({"library": library, "num-geometries": numGeometries, "points-per-geometry": n, "operation": "polygons", "reuse": False, "discard": True})
            allExperiments.append({"library": library, "num-geometries": numGeometries, "points-per-geometry": n, "operation": "polygons", "reuse": True, "discard": True})
          elif library == "esri":
            allExperiments.append({"library": library, "num-geometries": numGeometries, "points-per-geometry": n, "operation": "polygons", "reuse": False, "discard": False})
            allExperiments.append({"library": library, "num-geometries": numGeometries, "points-per-geometry": n, "operation": "polygons", "reuse": False, "discard": True})
            allExperiments.append({"library": library, "num-geometries": numGeometries, "points-per-geometry": n, "operation": "polygons", "reuse": True, "discard": True})
          elif library == "jts":
            for coordType in coordTypes:
              # Try with and without discard
              allExperiments.append({"library": library, "num-geometries": numGeometries, "points-per-geometry": n, "operation": "polygons", "discard": False, "coord": coordType})
              allExperiments.append({"library": library, "num-geometries": numGeometries, "points-per-geometry": n, "operation": "polygons", "discard": True, "coord": coordType})
    elif operation == "wkt":
      # 2- Test parsing WKT
      # 2.1- Parse WKT while keeping objects in memory (no discard option)
      inputFile = wkt_file
      num_polygons = [1000, 10000, 100000, 1000000, 15000000, 50000000, 200000000]
      #inputFile = "shapes1.wkt" # TODO Comment this line
      #num_polygons = [1000, 10000] # TODO comment this line
      for n in num_polygons:
        for library in libraries:
          if library == "geos":
            allExperiments.append({"library": library, "num-geometries": n, "input": inputFile, "operation": "wkt", "discard": False})
            allExperiments.append({"library": library, "num-geometries": n, "input": inputFile, "operation": "wkt", "discard": True})
          elif library == "geolite":
            allExperiments.append({"library": library, "num-geometries": n, "input": inputFile, "operation": "wkt", "discard": False, "reuse": False})
            allExperiments.append({"library": library, "num-geometries": n, "input": inputFile, "operation": "wkt", "discard": True, "reuse": False})
            allExperiments.append({"library": library, "num-geometries": n, "input": inputFile, "operation": "wkt", "discard": True, "reuse": True})
          elif library == "esri":
            allExperiments.append({"library": library, "num-geometries": n, "input": inputFile, "operation": "wkt", "discard": False})
            allExperiments.append({"library": library, "num-geometries": n, "input": inputFile, "operation": "wkt", "discard": True})
          elif library == "jts":
            for coordType in coordTypes:
              allExperiments.append({"library": library, "num-geometries": n, "input": inputFile, "operation": "wkt", "discard": False, "coord": coordType})
              allExperiments.append({"library": library, "num-geometries": n, "input": inputFile, "operation": "wkt", "discard": True, "coord": coordType})
    elif operation == "wkb":
      # 3- Parse WKB
      for n in num_polygons:
        for library in libraries:
          if library == "geos":
            allExperiments.append({"library": library, "num-geometries": n, "input": inputFile, "operation": "wkb", "discard": False})
            allExperiments.append({"library": library, "num-geometries": n, "input": inputFile, "operation": "wkb", "discard": True})
          elif library == "geolite":
            allExperiments.append({"library": library, "num-geometries": n, "input": inputFile, "operation": "wkb", "discard": False, "reuse": False})
            allExperiments.append({"library": library, "num-geometries": n, "input": inputFile, "operation": "wkb", "discard": True, "reuse": False})
            allExperiments.append({"library": library, "num-geometries": n, "input": inputFile, "operation": "wkb", "discard": True, "reuse": True})
          elif library == "esri":
            allExperiments.append({"library": library, "num-geometries": n, "input": inputFile, "operation": "wkb", "discard": False})
            allExperiments.append({"library": library, "num-geometries": n, "input": inputFile, "operation": "wkb", "discard": True})
          elif library == "jts":
            for coordType in coordTypes:
              allExperiments.append({"library": library, "num-geometries": n, "input": inputFile, "operation": "wkb", "coord": coordType, "discard": False})
              allExperiments.append({"library": library, "num-geometries": n, "input": inputFile, "operation": "wkb", "coord": coordType, "discard": True})
    elif operation == "rq":
      # 4- Range query experiments
      files = [wkt_file, join1_file, join2_file]
      for file in files:
        for library in libraries:
          if library == "geolite":
            allExperiments.append({"library": library, "input": file, "operation": "rq", "reuse": True})
            allExperiments.append({"library": library, "input": file, "operation": "rq", "reuse": False})
          elif library == "esri":
            allExperiments.append({"library": library, "input": file, "operation": "rq", "reuse": True})
            allExperiments.append({"library": library, "input": file, "operation": "rq", "reuse": False})
          elif library == "jts":
            for coordType in coordTypes:
              allExperiments.append({"library": library, "input": file, "operation": "rq", "coord": coordType})
    elif operation == "sj":
      # 5- Spatial join experiments
      input1 = join1_file
      input2 = join2_file
      for library in libraries:
        if library == "geolite":
          allExperiments.append({"library": library, "input1": input1, "input2": input2, "operation": "sj"})
        elif library == "esri":
          allExperiments.append({"library": library, "input1": input1, "input2": input2, "operation": "sj"})
        elif library == "jts":
          for coordType in coordTypes:
            allExperiments.append({"library": library, "input1": input1, "input2": input2, "operation": "sj", "coord": coordType})

  for experiment in allExperiments:
    runExperiment(experiment)