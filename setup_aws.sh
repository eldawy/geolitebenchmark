#!/usr/bin/sh

sudo apt update --fix-missing
sudo apt install g++ make python3 awscli screen openjdk-8-jre-headless

curl http://download.osgeo.org/geos/geos-3.8.1.tar.bz2 | bunzip2 | tar -x
cd geos-3.8.1
./configure --prefix=$HOME/Applications/geos/ --disable-dependency-tracking && make && make check && make install

# For r5ad.4xlarge machine with attached volumns
# Format the device
sudo mkfs -t xfs /dev/nvme0n1
# Create a mount directory
sudo mkdir /data
# Mount the volume
sudo mount /dev/nvme0n1 /data
# Change permission
cd /data
sudo chmod 777 .
# Download the files
aws s3 sync s3://edu.ucr.cs.bdlab.tmp/geolite/ ./
ln -s /data/all_objects.wkt $HOME/code/
cd $HOME/code
rm results_*
vi src/main/python/geolite_experiments.py
