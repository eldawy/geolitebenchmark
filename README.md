## Prerequisites
On Ubuntu, install the following to compile the C++ driver.
```shell script
sudo apt update --fix-missing
sudo apt install g++ make python3 awscli screen
```

## Setup instructions for the CPP part
* Install GEOS for your systems. Download the binaries from [https://trac.osgeo.org/geos/]. Follow the instructions at [https://trac.osgeo.org/geos/wiki/BuildingOnUnixWithAutotools]. The short version is below.
```shell
curl http://download.osgeo.org/geos/geos-3.8.1.tar.bz2 | bunzip2 | tar -x
cd geos-3.8.1
./configure --prefix=$HOME/Applications/geos/ --disable-dependency-tracking
make
make check
make install
```
* In your `gcc` command, add:
```shell
-I$HOME/Applications/geos/include
```
* In your `make` command, add:
```shell
-L$HOME/Applications/geos/lib -lgeos_c -lgeos
```
* Check the example at [https://github.com/libgeos/geos/blob/master/doc/example.cpp].
* Check the example at [https://github.com/libgeos/geos/blob/master/benchmarks/capi/IntersectionPerfTest.cpp].
* Check the list of functions at [https://geos.osgeo.org/doxygen/geos__c_8h_source.html].

## Install Spark (if not installed)
```shell script
curl https://mirrors.ocf.berkeley.edu/apache/spark/spark-2.4.5/spark-2.4.5-bin-hadoop2.7.tgz | tar -xz
echo 'export SPARK_HOME=$HOME/spark-2.4.5-bin-hadoop2.7' >> ~/.profile
echo 'export PATH=$PATH:$SPARK_HOME/bin' >> ~/.profile
```
